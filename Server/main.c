#include <stdio.h>
#include <stdlib.h>
#include "tcpserv.h"
#include "sctpserv.h"
#include <signal.h>

int main(int argc, char **argv)
{
    //setvbuf(stdout, NULL, _IONBF, 0);
    int choose;
    while(choose!=1 && choose!=2)
    {
        system("clear");
        printf("----------Linux Direct Remote Shell----------\n");
        printf("Choose type of server: \n");
        printf("[1]-TCP [2]-SCTP\n");
        scanf("%i", &choose);
        
        switch(choose)
        {
            case 1:
                tcpserv();
                break;
            case 2:
                sctpserv();
            default:
                break;
        } 

    }   
    return 0;
}