#include <stdio.h>
#include <stdlib.h>
#include "tcpclient.h"
#include "sctpserv.h"

int main(int argc, char **argv)
{
    setvbuf(stdout, NULL, _IONBF, 0);
    int choose;
    while(choose!=1 && choose!=2)
    {
        system("clear");
        printf("----------Linux Direct Remote Shell----------\n");
        printf("Choose type of client: \n");
        printf("[1]-TCP [2]-SCTP\n");
        scanf("%i", &choose);
        
        switch(choose)
        {
            case 1:
                tcpclient();
                break;
            case 2:
               // sctpclient();
            default:
                break;
        } 

    }   
    return 0;
}